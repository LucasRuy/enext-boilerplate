import gulp from 'gulp';
import stylus from 'gulp-stylus';
import utils from './utils';

gulp.task('stylus', () => {
	return gulp.src('app/static/stylesheet/application.styl')
		.pipe(stylus({ compress: false }).on('error', utils.consoleError))
		.pipe(gulp.dest('dist/assets/stylesheet'));

});
