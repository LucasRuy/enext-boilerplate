import gulp from 'gulp';

import imagemin from 'gulp-imagemin';
import svg from 'gulp-svg-sprite';

const images = './app/static/images/**/*';
const imagesSVG = './app/static/images/svg/*.svg';

gulp.task('copy:jsplugins', () => {
	gulp.src('./app/src/plugins/*.js')
	.pipe(gulp.dest('dist/assets/javascript'))
});

gulp.task('copy:images', () => {
	gulp.src('./app/static/images/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/assets/images'))
});

gulp.task('copy:svg', () => {
	return gulp.src(imagesSVG)
	.pipe(svg({
		mode: {
			symbol: {
				render: {
					css: false,
					scss: false
				},
				dest: 'svg',
				sprite: 'icons.svg',
			}
		}
	}))
	.pipe(gulp.dest('./dist/assets/images'));
});

gulp.task('copy:fonts', () => {
	gulp.src('./app/static/fonts/*')
	.pipe(gulp.dest('dist/assets/fonts'))
});

gulp.task('copy', ['copy:jsplugins', 'copy:images', 'copy:svg', 'copy:fonts']);