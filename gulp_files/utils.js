const utils = {
    consoleError: function(msg){
    		console.log('\n \n');
        console.log(' Plugin: ', msg.plugin);
        console.log(' File: ', msg.filename);
        console.log(' Line: ', msg.lineno);
        console.log(' Error: ', msg.stack);
        console.log('\n \n');
        this.emit('end');
    }
};

export default utils;