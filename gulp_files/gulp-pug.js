import gulp from 'gulp';
import pug from 'gulp-pug';
import utils from './utils';

gulp.task('pug', () => {
	return gulp.src('app/static/templates/*.pug')
		.pipe(pug({ pretty: true}).on('error', utils.consoleError))
		.pipe(gulp.dest('./dist/'));
});