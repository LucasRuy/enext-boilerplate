import gulp from 'gulp';

gulp.task('watch', () => {
	gulp.watch(	['app/static/templates/index.pug', 'app/static/templates/_views/!(_)*.pug', 'app/static/templates/_includes/!(_)*.pug'], ['pug'] );
	gulp.watch( 'app/static/templates/**/*.pug', ['pug'] );
	gulp.watch( 'app/static/stylesheet/**/*.styl', ['stylus'] );
	gulp.watch( 'app/src/**/*.js', ['bundle'] );
});