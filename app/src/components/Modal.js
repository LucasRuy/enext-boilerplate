import { get } from '../utils';

const Modal = (btn, content) => {
	const modalOpen = get.element(btn);
	const modalSelector = get.element(content);

	modalOpen.addEventListener('click', () => {
		const modalWrapper = document.createElement('div');
		const modalContent = document.createElement('div');
		const modalClose = document.createElement('button');

		modalWrapper.classList.add('modal-wrapper');
		modalContent.classList.add('modal-content');
		modalClose.classList.add('modal-close');
		modalClose.setAttribute('title', 'Minimizar');

		modalContent.appendChild(modalSelector);
		modalContent.appendChild(modalClose);
		modalWrapper.appendChild(modalContent);

		document.body.appendChild(modalWrapper);
		modalWrapper.classList.add('is-active');

		modalClose.addEventListener('click', () => {
			modalWrapper.parentNode.removeChild(modalWrapper);
			modalWrapper.classList.remove('is-active');
		});
	});
};

export default Modal;